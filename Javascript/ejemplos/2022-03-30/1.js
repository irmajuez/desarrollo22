window.addEventListener("load", () => {//esperar que cargue la pagina
    document.querySelector('div').innerHTML = localStorage.getItem("numeros");//colocar en el div de abajo lo que tenga el dato(los num que escribo)

    document.querySelector('#borra').addEventListener("click", () => {
        localStorage.removeItem("numeros");//me borra numeros
        document.querySelector('div').innerHTML = localStorage.getItem("numeros");
    });


    document.querySelector('#mas').addEventListener("click", () => {
        let numero = document.querySelector('#numero').value;
        let numeros="";
        let vectorNumeros=[];
        if (localStorage.getItem("numeros") != null) {//compruebo si habia metido algo en numeros
            numeros = localStorage.getItem("numeros") + ", " + numero;//cojo lo que habia en numeros y le añado el nuevo numero
        }else{
            numeros=numero;
        }

        localStorage.setItem("numeros", numeros);
        document.querySelector('div').innerHTML = numeros;

        //calculo la media
        vectorNumeros=numeros.split(", ");
        let suma=0;
        let media=0;
        vectorNumeros.forEach(function(v){
            suma+=parseInt(v);
        });
        media=suma/vectorNumeros.length;
        document.querySelector('div').innerHTML+=`<br>${media}`;
    })
})


