window.addEventListener("load", () => {//esperar que cargue la pagina
    document.querySelector('div').innerHTML = localStorage.getItem("numeros");
    //colocar en el div de abajo lo que tenga el dato(los num que escribo)

    document.querySelector('#borra').addEventListener("click", () => {
        localStorage.removeItem("numeros");//me borra numeros
        document.querySelector('div').innerHTML = localStorage.getItem("numeros");
    });


    document.querySelector('#mas').addEventListener("click", () => {
        let numero = document.querySelector('#numero').value;
        let numeros=[];
        
        if (localStorage.getItem("numeros") != null) {
            //compruebo si habia metido algo en numeros
            numeros = JSON.parse(localStorage.getItem("numeros"));
            //creando un objeto del string
        }

        numeros.push(numero);

        localStorage.setItem("numeros", JSON.stringify(numeros));
        document.querySelector('div').innerHTML = numeros;

        //calculo la media
        let suma=0;
        let media=0;

        numeros.forEach(function(v){
            suma+=parseInt(v);
        });
        media=suma/numeros.length;
        document.querySelector('div').innerHTML+=`<br>${media}`;
    })
})


