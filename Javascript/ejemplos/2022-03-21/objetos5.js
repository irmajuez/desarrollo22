/**
 * Crear una clase denominada Perro
 * Perro1 nomenclatura antigua
 * Perro2 nomenclatura moderna (ES5)
 */

/**
 * Propiedades:
 * color
 * pelo
 * peso
 * fechaNacimiento
 */

/**
 * Metodos:
 * ladrar("gua gua")
 * dormir("durmiendo")
 */

let Perro1=function(){
    this.constructor=function(){
        this.color="marron";
        this.pelo=true;
        this.peso=20;
        this.fechaNacimiento="20/1/2020";
        console.log("Creando perro");
    }

    this.ladrar=function(){
        return "gua gua";
    }

    this.dormir=function(){
        return "durmiendo";
    }

    this.constructor();
}

//creamos un objeto de tipo Perro1
let perro1=new Perro1();


class Perro2{
    constructor(){
        this.color="negro";
        this.pelo=true;
        this.peso=10;
        this.fechaNacimiento="15/4/2021";
        console.log("Creando perro");
    }

    ladrar(){
        return "gua gua";
    }

    dormir(){
        return "durmiendo";
    }
}

//creamos un objeto de tipo Perro2
let perro2=new Perro2();

