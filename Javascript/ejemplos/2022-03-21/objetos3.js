//creando una clase de tipo Loro
let Loro=function(){
    //metodo constructor
    this.constructor=function(){
        //creando propiedades publicas
        this.color="rojo";
        this.peso=10;
        console.log("creando loro");
    }

    //creando metodos publicos 
    this.hablar=function(){
        return "piopio";
    }

    this.volar=function(){
        return "volando voy";
    }

    //llamando al constructor
    this.constructor();
}

/**
 * creo dos objetos de tipo Loro
 */

let loro1=new Loro();
let loro2=new Loro();

loro1.color="Azul";
