let posiciones=[1,5,9,8,7,3,2];
let total=0;


/**
 * funcion que recibe un array de numeros y 
 * devuelve el producto de todos
 * los numeros pares 
 */
function productoPares(numeros){
    let resultado=1;
    let c=0;
    for(c=0;c<numeros.length;c++){
        if((numeros[c]%2)==0){
            resultado*=numeros[c];
        }
    }
    return resultado;
}
total=productoPares(posiciones);//8*2
console.log(total);

/**
 * funcion que recibe un array de numeros
 * y retorna la suma de todos los numeros 
 * impares del array
 */
function sumaImpares(numeros){
    let resultado=0;
    let c=0;
    for(c=0;c<numeros.length;c++){
        if((numeros[c]%2)==!0){
            resultado+=numeros[c];
        }
    }
    return resultado;
}
total=sumaImpares(posiciones);//1+5+9+7+3
console.log(total);