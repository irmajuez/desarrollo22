/*PARA NO USAR EL GETCOMPUTERSTYLE:
let pantalla=document.querySelector('#display');
pantalla.style.left="0px";
pantalla.style.top="0px";*/

function escribir(argumento){
    //alert(argumento);PRUEBA de que funciona
    let pantalla=document.querySelector('#display');
    
    if(argumento=='c'){
        pantalla.innerHTML="";
    }else{
        pantalla.innerHTML+=argumento;
    }
}

function mover(direccion){
    let pantalla=document.querySelector('#display');
    const PASO=10;

    let izquierda=window.getComputedStyle(pantalla).getPropertyValue("left");

    let arriba=window.getComputedStyle(pantalla).getPropertyValue("top");

    if(direccion=='derecha'){
        pantalla.style.left=parseInt(izquierda)+PASO+"px";
    }else if(direccion=='abajo'){
        pantalla.style.top=parseInt(arriba)+PASO+"px";
    }
}
