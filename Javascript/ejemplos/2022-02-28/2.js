function producto(numero1,numero2){
    let resultado=0;//variable local
    resultado=numero1*numero2;
    return resultado;
}

//variables globales
let multi=0;
let num1=0;
let num2=0;

//asignando valor a las variables
num1=5;
num2=10;

multi=producto(num1,num2);

producto();

//Aqui no puedo utilizar lasvariables creadas dentro de la funcion
////document.write("El valor de la variable num1 es "+num1+"<br>");
document.write(`El valor de la variable num1 es ${num1} <br>`);
//document.write("El valor de la variable num2 es "+num2+"<br>");
document.write(`El valor de la variable num2 es ${num2} <br>`);
//document.write(num1+" * "+num2+" = "+multi+"<br>");
document.write(`${num1} * ${num2} = ${multi} <br>`);

///////////////////////////////////////////////////////////////////////////////


function suma(numeros){
    let resultado=0;
    let c=0;

    for(c=0;c<numeros.length;c++){
        if((numeros[c]%2)==0){
            //es par
            //resultado=resultado+numeros[c];
            resultado+=numeros[c];
        }
    }
    return resultado;
}

let valores=[2,3,2,6];
r=suma(valores);
document.write(`El resultado de la suma de los numeros pares es ${r}`);