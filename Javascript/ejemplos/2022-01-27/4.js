/*
Colocando variables
*/
let radio=10;
var resultado=0;

/**
 Creando constantes
 **/
 const PI=3.14;

 /**
  Procesando
 **/

 resultado=2*PI*radio; //colocamos una expresion

 document.write("Contenido con Javascript <br>");

 document.write("El resultado es " + resultado);

 console.log(resultado);
