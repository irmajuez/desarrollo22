class Animal {
    // miembro privado
    #
    tratamiento = "Amado";#
    comido = 0;
    tipo = "patito";

    get nombre() {
        return this._nombre;
    }
    set nombre(n) {
        this._nombre = n;
    }
    hablar() {
        return `Cuak soy un ${this.tipo}`;
    }
    despedirse() {
        return `Adios ${this.#tratamiento} patito`;
    }
    comer() {
        this.#vivir();
        return `He comido ya ${this.#comido} veces`;
    }

    #
    vivir() {
        this.#comido++;
    }
    constructor(n = "pato") {
        this.nombre = n;
        console.warn("madre mia un patito mas");
    }
    quienSoy() {
        return `Hola soy ${this.nombre}`;
    }
}

let pato1 = new Animal("croqueta");
pato1.nombre = "jose";
console.log(pato1.nombre);
console.log(pato1.despedirse());
console.log(pato1.hablar());
console.log(pato1.comer());
console.log(pato1.comer());