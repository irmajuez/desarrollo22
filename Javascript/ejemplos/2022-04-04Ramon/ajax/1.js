document.querySelector('button').addEventListener('click', () => {
    const data = new FormData(document.querySelector('form'));

    fetch('1.php', {
            method: 'post',
            body: data
        })
        .then(function(response) {
            if (response.ok) {
                return response.text()
            } else {
                throw "Error en la llamada Ajax";
            }

        })
        .then(function(texto) {
            document.querySelector('div').innerHTML += `<br>${texto}`;
        })
        .catch(function(err) {
            console.log(err);
        });

    const dato = {
        empresa: 'Alpe Formacion',
        CIF: '20202020a',
        Formacion: 'Desarrollo web'
    }

    fetch('2.php', {
            method: 'post',
            body: JSON.stringify(dato),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(function(response) {
            if (response.ok) {
                return response.json()
            } else {
                throw "Error en la llamada Ajax";
            }

        })
        .then(function(texto) {
            document.querySelector('tbody').innerHTML += `<tr><td>${texto.empresa}</td><td>${texto.CIF}</td><td>${texto.Formacion}</td></tr>`;
            console.log(texto);

        })
        .catch(function(err) {
            console.log(err);
        });

    const datos = new FormData();
    datos.append('colores', JSON.stringify({ frio: 'rojo', calido: 'azul' }));

    fetch('3.php', {
            method: 'post',
            body: datos,
        })
        .then(function(response) {
            if (response.ok) {
                return response.json()
            } else {
                throw "Error en la llamada Ajax";
            }

        })
        .then(function(texto) {
            console.log(texto);

        })
        .catch(function(err) {
            console.log(err);
        });


});