let alumnos=[];

document
    .querySelector('#mas')
    .addEventListener("click",function(){
        let alumno={
            nombre:document.querySelector('#nombre').value,
            nif:document.querySelector('#nif').value,
            edad:document.querySelector('#edad').value
        };

        if(localStorage.getItem("kalumnos")!=null){
            alumnos=JSON.parse(localStorage.getItem("kalumnos"))
        }

        alumnos.push(alumno);
        localStorage.setItem("kalumnos",JSON.stringify(alumnos));

    });

document
    .querySelector("#borra")
    .addEventListener("click",function(){
        alumnos=[];
        localStorage.removeItem("kalumnos");
    });

