let pulsado=false;
canvasElement.onmousedown=function(e){
    pulsado=true;
}

canvasElement.onmouseup=function(e){
    pulsado=false;
}
canvasElement.onmousemove = function(e) {
    let ctx = canvasElement.getContext('2d');
    if(pulsado){
        ctx.lineTo(e.clientX, e.clientY);
        ctx.stroke();
    }else{
        ctx.moveTo(e.clientX, e.clientY);
    }
    
  };