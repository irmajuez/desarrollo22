//apuntamos a los dos div
let divs=document.querySelectorAll('div');

//recorro los dos div con un for
for(let c=0;c<divs.length;c++){
    console.log(divs[c].innerHTML);
}

//recorro los dos div con un for of
for(let valor of divs){
    console.log(valor.innerHTML);
}

//recorro los dos div con fon in (para objetos)
for(let indice in divs){
    console.log(divs[indice].innerHTML);
}

//recorro los dos div con foreach (para arrays)
divs.forEach(function(valor,indice){
    console.log(valor.innerHTML);
});
