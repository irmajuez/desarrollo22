let persona={
    nombre:"Irma",
    altura:160,
    saludar:function(){
        return "Hola";
    }
}

//Hacen lo mismo, pero mejor usar la segunda linea de cada:
console.log(persona['nombre']);
console.log(persona.nombre);

console.log(persona['saludar']());
console.log(persona.saludar());


//Recorrer con for in
for(let miembro in persona){
    if(typeof(persona[miembro])!='function'){
        console.log(persona[miembro]);
    }else{
        console.log(persona[miembro]());
    }
}