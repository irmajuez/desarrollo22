/**
 * recibe una serie de numeros
 * {
 * vector1:[2,3,4,5]
 * vector2:[2,3,4,6]
 * vector3:"texto"
 * }
 * 
 * la funcion me tiene que devolver un objeto
 * de tipo
 * {
 * suma:[4,6,8,11]
 * producto:[4,9,16,30]
 * max:6 el numero mas alto de los dos vectores
 * }
 * 
 * me debe mostrar el valor mas grande
 * en el elemento con id con el valor de salida
 */


function operaciones(datos){
    let resultado={
        suma:[],
        producto:[],
        max:0
    }


    /*
    for(let i=0;i<datos.vector1.length;i++){
        resultado.suma[i]=datos.vector1[i]+datos.vector2[i];
    }
    */

    //calulamos suma y producto
    datos.vector1.forEach(function(v,i){
        resultado.suma[i]=datos.vector1[i]+datos.vector2[i];
        resultado.producto[i]=datos.vector1[i]*datos.vector2[i];
    });

    //calculamos el maximo
    resultado.max=Math.max(...datos.vector1,...datos.vector2);

    //mostramos el maximo en el div
    document.querySelector('#' + datos.salida).innerHTML=resultado.max;

    return resultado;
}

let origen={
    vector1:[2,3,4,5],
    vector2:[2,3,4,6],
    salida:"texto"
}

console.log(operaciones(origen));


console.log(operaciones({
    vector1:[5,6,8],
    vector2:[89,2,3],
    salida:"barra"
}));