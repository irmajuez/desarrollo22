let carpeta='imgs/';
let fotos=[
    'descarga.png', 
    'descarga2.png', 
    'descarga3.jpg'
];

//SOLUCION MIA:
//document.write('<img src="' + fotos[0] + '">' + "<br>");
//document.write('<img src="' + fotos[1] + '">' + "<br>");
//document.write('<img src="' + fotos[2] + '">' + "<br>");

//SOLUCION 1:
document.write('<img src="' 
    + carpeta 
    + fotos[0] 
    + '">'
    + "<br>");
document.write('<img src="' 
    + carpeta 
    + fotos[1] 
    + '">'
    + "<br>");
document.write('<img src="' 
    + carpeta 
    + fotos[2] 
    + '">'
    + "<br>");

//SOLUCION 2:
document.write(`<img src="${carpeta}${fotos[0]}">` + "<br>");
document.write(`<img src="${carpeta}${fotos[1]}">` + "<br>");
document.write(`<img src="${carpeta}${fotos[2]}">` + "<br>");
