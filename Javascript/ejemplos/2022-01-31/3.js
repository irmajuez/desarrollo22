//Definicion de variables
var radio=0;
var perimetro=0;
var area=0;

//Introducir datos
radio=prompt("Introduce radio","0");

//Procesamiento de la informacion
perimetro=2*Math.PI*radio;
area=Math.PI*Math.pow(radio,2);
//area=Math.PI*radio**2; Con operador

//Mostrar resultados
document.write("El radio es: " + radio + "<br>");
document.write("El perimetro es: " + perimetro + "<br>");
document.write("El area es: " + area);
//document.write(`El area es${area}<br>`);

