//Definicion de variables
let base=0;
let altura=0;
let perimetro=0;
let area=0;

//Introducir datos
base=prompt("Introduce la base");
altura=prompt("Introduce la altura");

//Procesamiento de la informacion
perimetro=2*base+2*altura;
area=base*altura;

//Mostrar informacion
document.write("El perimetro es: " + perimetro + "<br>");
//document.write(`El perimetro es ${perimetro}<br>`);
document.write("El area es: " + area);
//document.write(`El area es ${area}<br>`);

