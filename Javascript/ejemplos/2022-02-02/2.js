//Definicion de variables
let radio=0;
let perimetro=0;
let area=0;
let circulo=null;//apunta al div circulo

//Introducir datos
radio=prompt("Introduce radio","0");

//Procesamiento de la informacion
perimetro=2*Math.PI*radio;
area=Math.PI*Math.pow(radio,2);

//Mostrar resultados
document.write("El radio es: " + radio + "<br>");
document.write("El perimetro es: " + perimetro + "<br>");
document.write("El area es: " + area);

//Dibujar circulo con radio dado
circulo=document.querySelector("#circulo"); //#circulo porque en el div esta con ID
circulo.style.width=radio+"px";
circulo.style.height=radio+"px";
circulo.style.backgroundColor="purple";
circulo.style.borderRadius=radio+"px";



