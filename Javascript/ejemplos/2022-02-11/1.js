let promedio=parseInt(prompt("Introduce tu promedio"));//pido nota media alumno
let descuento=0;//descuento en moneda
let pago=0;//pago en moneda

//Crear variables que apuntan a los span
let cInsc=null;
let cDesc=null;
let cPago=null;

//Constantes a utilizar
const INSCRIPCION=3000;
const PDESCUENTO=0.2;

//Asigno variables a span
cInsc=document.querySelector('#ins');
cDesc=document.querySelector('#desc');
cPago=document.querySelector('#pago');


if(promedio>=9){
    descuento=INSCRIPCION*PDESCUENTO;
}

pago=INSCRIPCION-descuento;

//Mostrar datos
cDesc.innerHTML=descuento;
//document.write(descuento); Es lo mismo pero sin situarlo en el span
cPago.innerHTML=pago;