function encender(){
    document.querySelector("img").style.display='inline-block';
    //document.querySelector("img").style.visibility='visible';
    document.querySelector("img").src="img/on.gif";
    document.querySelector("img").alt="bombilla encendida";
}

function apagar(){
    document.querySelector("img").style.display='inline-block';
    document.querySelector("img").src="img/off.gif";
    document.querySelector("img").alt="bombilla apagada";
}

function eliminar(){
    document.querySelector("img").style.display='none';
    //document.querySelector("img").style.visibility='hidden';
}