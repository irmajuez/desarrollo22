/**
 * clase llamada Persona con los miembros
 * nombre
 * edad
 */


//CON CLASE (MEJOR ESTA OPCION)
/*class Persona{
    constructor(){
        this.nombre="";
        this.edad=0;
        this.localidad="";
    }

}*/


//CON FUNCION
const Persona=function(){
    this.constructor=function(){
        this.nombre="";
        this.edad=0;
        this.localidad="";
    }
    this.constructor();
}

const personas=[];
personas.push(new Persona(),new Persona());

personas[0].nombre="Ana Gonzalez";
personas[1].nombre="Luis Gomez";
