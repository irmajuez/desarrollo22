/**
 * clase llamada Persona con los miembros
 * nombre
 * edad
 */



/*class Persona{
    constructor(nombre="",edad=0,localidad=""){//valor predeterminado para que no de error si no se pasan esos argumentos
        this.nombre=nombre;
        this.edad=edad;
        this.localidad=localidad;
    }

}*/



const Persona=function(nombre="",edad=0,localidad=""){
    this.constructor=function(){
        this.nombre=nombre;
        this.edad=edad;
        this.localidad=localidad;
    }
    this.constructor(nombre,edad,localidad);
}



const personas=[];
personas.push(
    new Persona(),
    new Persona("ana gonzalez"),
    new Persona("luis gomez",45)
);

personas[0].nombre="Ana Gonzalez";
personas[1].nombre="Luis Gomez";
