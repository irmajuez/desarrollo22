/**
 * clase llamada Persona con los miembros
 * nombre
 * edad
 */



class Persona{
    constructor(datos={}){//JSON
        /*if(datos.hasOwnProperty("nombre")){
            this.nombre=datos.nombre;
        }else{
            this.nombre="";
        }*/

        //OPCION 1:
        this.nombre=datos.hasOwnProperty("nombre")? datos.nombre: "";//OPERADOR TERNARIO (IGUAL QUE CON IF ELSE)

        this.edad=datos.hasOwnProperty("edad")? datos.edad: 0;

        this.localidad=datos.hasOwnProperty("localidad")? datos.localidad: "";

        if(datos.hasOwnProperty("nombre") || (datos.nombre="")){
            this.nombre=datos.nombre;
        }

        //OPCION 2:
        datos.hasOwnProperty("nombre") || (datos.nombre="");
        datos.hasOwnProperty("edad") || (datos.edad="");
        datos.hasOwnProperty("localidad") || (datos.localidad="");
        this.nombre=datos.nombre;
        this.edad=datos.edad;
        this.localidad=datos.localidad;
    }

}


const personas=[];
personas.push(
    new Persona(),
    new Persona({
        nombre:"ana gonzalez"
    }),
    new Persona({
        nombre:"luis gonzalez",
        edad:45
    })
);

