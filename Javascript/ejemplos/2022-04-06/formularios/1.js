class Dato{
    constructor(id=0,nombre='',email='',edad=0,poblacion=''){
        this.id=id;
        this.nombre=nombre;
        this.email=email;
        this.edad=edad;
        this.poblacion=poblacion;
    }
    imprimir(etiqueta){
        let resultado="";
        resultado+=`
        <${etiqueta}>${this.id}</${etiqueta}>
        <${etiqueta}>${this.nombre}</${etiqueta}>
        <${etiqueta}>${this.email}</${etiqueta}>
        <${etiqueta}>${this.edad}</${etiqueta}>
        <${etiqueta}>${this.poblacion}</${etiqueta}>
        `;

        return resultado;
    }

    static cabeceras(){
        let resultado = "";
        let etiqueta = 'td';
        resultado += `
        <${etiqueta}>id</${etiqueta}>
        <${etiqueta}>nombre</${etiqueta}>
        <${etiqueta}>email</${etiqueta}>
        <${etiqueta}>edad</${etiqueta}>
        <${etiqueta}>poblacion</${etiqueta}>   
        `;
        return resultado;

        /*for (let titulo in new this()) {
            resultado += `<td>${titulo}</td>`;
        }
        return resultado;*/

    }
}

let datos=[];

/*function cabeceras(objeto){
    let resultado="";
    for(let titulo in objeto){
        resultado+=`<td>${titulo}</td>`;
    }
    return resultado;
}*/

let header=Dato.cabeceras();
document.querySelector('thead>tr').innerHTML=header;


document.querySelector('#nuevo').addEventListener("click",function(e){
    /*let dato={
        id:null,
        nombre:null,
        email:null,
        edad:null,
        poblacion:null
    }*/

    let dato=new Dato(
        document.querySelector('#id').value,
        document.querySelector('#nombre').value,
        document.querySelector('#email').value,
        document.querySelector('#edad').value,
        document.querySelector('#poblacion').value
    );

    document.querySelector('tbody').innerHTML+=`<tr>${dato.imprimir('td')}</tr>`;


    datos.push(dato);
    console.log(datos);
});

