/*window.getComputedStyle(document.querySelector('#salida').getPropertyValue('width'));
document.querySelector('#salida').style.width="400px";*/
let vSalida=document.querySelector('#salida');
let anchura=window.getComputedStyle(vSalida).getPropertyValue('width');

function cambiar(color){
    let muestra=document.querySelector('#salida');
    muestra.style.backgroundColor=color;
}

function ancho(valor){
    let muestra=document.querySelector('#salida');
    //leo el ancho del div independientemente de como se haya colocado el estilo
    let ancho=window.getComputedStyle(muestra).getPropertyValue('width');

    //leo el ancho del div si se lo he colocado en linea
    //let ancho=muestra.style.width;

    muestra.style.width=(parseInt(ancho)+valor).toString()+"px";
}