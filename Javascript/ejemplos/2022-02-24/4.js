//Fijando la anchura desde JavaScript
document.querySelector('#salida').style.width="400px";

function rojo(){
    let dato=document.querySelector('#salida');
    dato.style.backgroundColor="red";
}
function verde(){
    let dato=document.querySelector('#salida');
    dato.style.backgroundColor="green";
}
function azul(){
    let dato=document.querySelector('#salida');
    dato.style.backgroundColor="blue";
}
function incAncho(){
    let dato=document.querySelector('#salida');
    dato.style.width=parseInt(dato.style.width)+10+"px";
}
function decAncho(){
    let dato=document.querySelector('#salida');
    dato.style.width=parseInt(dato.style.width)-10+"px";
    //dato.style.width=(parseInt(dato.style.width)-10).toString()+"px";=> con toString se pasa a texto el numero
}
