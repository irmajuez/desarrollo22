function rojo(){
    let muestra=document.querySelector('#salida');
    muestra.style.backgroundColor="red";
}
function verde(){
    let muestra=document.querySelector('#salida');
    muestra.style.backgroundColor="green";
}
function azul(){
    let muestra=document.querySelector('#salida');
    muestra.style.backgroundColor="blue";
}
function incAncho(){
    let muestra=document.querySelector('#salida');
    let ancho=window.getComputedStyle(muestra).getPropertyValue("width");//leo el ancho de mi div
    muestra.style.width=parseInt(ancho)+10+"px";
}
function decAncho(){
    let muestra=document.querySelector('#salida');
    let ancho=window.getComputedStyle(muestra).getPropertyValue("width");//leo el ancho de mi div
    muestra.style.width=parseInt(ancho)-10+"px";
}