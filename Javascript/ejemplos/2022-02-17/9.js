let num=+prompt("Introduce un número del 1 al 7");
let dia=0;

//ELSE IF
if(num==1){
    dia="Lunes";
}else if(num==2){
    dia="Martes";
}else if(num==3){
    dia="Miércoles";
}else if(num==4){
    dia="Jueves";
}else if(num==5){
    dia="Viernes";
}else if(num==6){
    dia="Sábado";
}else if(num==7){
    dia="Domingo";
}

if(dia==0){
    document.write("Error");
}else{
    document.write(dia+"<br>");
}


//SWITCH
switch (num){
    case 1:
        dia="Lunes";
        break;
    case 2:
        dia="Martes";
        break;
    case 3:
        dia="Miércoles";
        break;
    case 4:
        dia="Jueves";
        break;
    case 5:
        dia="Viernes";  
        break;
    case 6:
        dia="Sábado"; 
        break;
    case 7:
        dia="Domingo";     
        break;
    default:
        dia=0;
        break;
}

if(dia==0){
    document.write("Error");
}else{
    document.write(dia);
}

