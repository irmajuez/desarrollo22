const Animal=function(){
    //propiedad publica
    this.peso=0;

    //propiedad estatica
    Animal.pelo="si";

    //metodo publico
    this.comer=function(){
        console.log("ÑAN");
    };

    //metodo publico
    this.tienePelo=function(){
        /*Dentro de un metodo no estatico si podemos acceder a miembros estaticos*/
        console.log("tiene pelo?" + Animal.pelo);
    };

    //metodo estatico
    Animal.estatico=function(){
        console.log("esto es un elemento de la clase");
        /*dentro de un elemento estatico no puedo llamar a elementos no estaticos this.comer();*/
        console.log(Animal.pelo);
    };
};

var leon=new Animal();
/*Produce error ya que es un metodo de la clase y no del objeto leon.estatico();*/
Animal.estatico();
console.log(Animal.pelo);
leon.tienePelo();
/*Esto esta vacio porque es un elemento de la clase y no del objetoconsole.log(leon.pelo);*/