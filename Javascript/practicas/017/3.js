const Operaciones=function(num1,num2){
    this.num1=num1;
    this.num2=num2;
    this.resultado=0;

    this.sumar=function(){
        this.resultado=this.num1+this.num2;
    };
    this.restar=function(){
        this.resultado=this.num2-this.num1;
    };
    this.producto=function(){
        this.resultado=this.num2*this.num1;
    };
    this.getResultado=function(){
        return this.resultado;
    };
};

const operacion1=new Operaciones(1,2);
operacion1.sumar();
console.log(operacion1.getResultado());
operacion1.restar();
console.log(operacion1.getResultado());
