let canvas;
        let ctx;

        window.addEventListener("load",inicio);

        function inicio(){
            canvas=document.querySelector("canvas");
            ctx=canvas.getContext("2d");
            dibujo5();
        }

        function dibujo5(){
            for(let x=10;x<600;x+=60){
                ctx.fillStyle="rgb(200,0,0)";
                ctx.fillRect(10+x,10,55,50);
                ctx.fillStyle="rgba(0,0,200,0.5)";
                ctx.fillRect(10+x,80,55,50);
            }
        }