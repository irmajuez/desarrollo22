let canvas;
        let ctx;

        window.addEventListener("load",inicio);

        function inicio(){
            canvas=document.querySelector("canvas");
            ctx=canvas.getContext("2d");
            dibujo8();
        }

        function dibujo8(){
            //triangulo relleno
            ctx.beginPath();
            ctx.moveTo(25,25);
            ctx.lineTo(105,25);
            ctx.lineTo(25,105);
            ctx.fill();

            //triangulo contorneado
            ctx.beginPath();
            ctx.moveTo(125,125);
            ctx.lineTo(125,45);
            ctx.lineTo(45,125);
            ctx.closePath();
            ctx.stroke();
        }