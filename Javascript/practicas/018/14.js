let canvas;
        let ctx;

        window.addEventListener("load",inicio);

        function inicio(){
            canvas=document.querySelector("canvas");
            ctx=canvas.getContext("2d");
            dibujo12();
        }

        function dibujo12(){
            let colores=new Array("RED","BLUE","GREEN");
            for(let c=15,d=0;c<345;c+=15,d++){
                ctx.beginPath();
                ctx.fillStyle=colores[d%3];
                ctx.arc(75,75,50,c*Math.PI/180,(c+15)*Math.PI/180,false);
                ctx.fill();
            }
        }