class Div{
    //privado
    #texto="hola";

    //publico
    constructor(){
        this._borde=1;
        this.fondo="RED";
    }

    get borde(){
        return this._borde;
    }

    set borde(valor){
        this._borde=valor;
    }

    get texto(){
        return this.#texto;
    }

    set texto(valor){
        this.#texto=valor;
    }

    get fondo(){
        return `backgroundColor=${this._fondo}`;
    }

    set fondo(valor){
        this._fondo=valor;
    }

}

const caja1=new Div();
console.log(caja1.borde);