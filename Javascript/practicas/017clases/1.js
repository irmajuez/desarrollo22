class Animal{
    //propiedad no estatica
    static pelo="si";

    constructor(){
        //propiedad no estatica
        this.peso=0;
    }
    //metodo no estatico
    comer(){
        console.log("ÑAN");
        console.log(Animal.pelo);
    }

    //metodo estatico
    static estatico(){
        this.peso=100;
        console.log("esto es un elemento de la clase");
    };
};

const leon=new Animal();
/*Produce error ya que es un metodo de la clase y 
no del objeto leon.estatico();*/
Animal.estatico();
console.log(Animal.pelo);
/*Esto esta vacio porque es un elemento de la clase
y no del objeto console.log(leon.pelo);*/