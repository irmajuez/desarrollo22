let numeros=[5,8];

if(numeros[0]<numeros[1]){
    console.log(numeros[0] + " no es mayor que " + numeros[1]);
}

if(numeros[1]>=0){
    console.log(numeros[1] + " es positivo");
}

if(numeros[0]<0 || numeros[1]!=0){
    console.log(numeros[0] + " es negativo o distinto de 0");
}

if(numeros[0]+1>numeros[1]){
    console.log("Incrementar en 1 unidad el valor de " + numeros[0] + " no lo hace mayor o igual que " + numeros[1]);
}