let dni=parseInt(prompt("Introduce tu número de DNI"));
let letradni="";
let tuletra="";

let letras=[
    'T',
    'R',
    'W',
    'A',
    'G',
    'M',
    'Y',
    'F',
    'P',
    'D',
    'X',
    'B',
    'N',
    'J',
    'Z',
    'S',
    'Q',
    'V',
    'H',
    'L',
    'C',
    'K',
    'E',
    'T'
];

if(dni<0 || dni>99999999){
    document.write("El número de DNI proporcionado no es válido");
}else{
    letradni=letras[dni%23];
    document.write("La letra de tu DNI es " + letradni +"<br>");
}

tuletra=prompt("Introduce tu letra de DNI en mayúscula");

if(letradni!=tuletra){
    document.write("La letra que has indicado no es correcta");
}else{
    document.write("El número y la letra de DNI indicados son correctos");
}