let radio;
let longitud;
let area;
let volumen;

radio=parseInt(prompt("Introduce un radio"));

longitud=2*Math.PI*radio;
area=Math.PI*Math.pow(radio,2);
volumen=4/3*Math.PI*Math.pow(radio,3);

document.write("La longitud es: " + longitud + "<br>");
document.write("El área es: " + area + "<br>");
document.write("El volumen es: " + volumen + "<br>");
