/*Vamos a crear una clase que recibe 3 argumentos*/
 var caja=function(a,b,c){

    //propiedad privada
    var unidad="px";
    
     //propiedades publicas
     this.ancho=a;
     this.alto=b;
     this.texto=c;

     //metodo privado
     function concatenar(){
         this.ancho=this.ancho+unidad;
         this.alto=this.alto+unidad;
     }
     
     //metodos publicos
     this.mensaje=function(){
        this.texto="esto es un ejemplo";
     };
     this.mostrar=function(){
         alert(this.texto);
     };
 };