/*Creamos la clase*/

const Caja=function(ancho,alto,texto){
    //propiedad privada
    let unidad="px";

    //propiedades publicas
    this.ancho=ancho;
    this.alto=alto;
    this.texto=texto;

    //metodo privado
    function concatenar(){
        this.ancho=this.ancho+unidad;
        this.alto=this.alto+unidad;
    }

    //metodos publicos
    this.mensaje=function(){
        this.texto="Esto es un ejemplo";
    };
    this.mostrar=function(){
        //concatenar(); Para que se pueda usar
        alert(this.texto);
    };

};

//creamos el objeto

const objeto=new Caja(10,20,"hola mundo");

console.log(objeto.alto);
objeto.mostrar();

