let frase=prompt("Introduce una frase");
let frasemin=frase.toLowerCase();
let vector=frasemin.split("");
let contadorvocales=0;
let contadorconson=0;
let contadorespa=0;

for(let c=0; c<vector.length; c++){
    if( vector[c]=='a'||
        vector[c]=='á'||
        vector[c]=='e'||
        vector[c]=='é'||
        vector[c]=='i'||
        vector[c]=='í'||
        vector[c]=='o'||
        vector[c]=='ó'||
        vector[c]=='u'||
        vector[c]=='ú'
    ){
        contadorvocales++;
    }else if(vector[c]==" "){
        contadorespa++;
    }

    contadorconson=vector.length-contadorespa-contadorvocales;
    
}

document.write("La frase " + frasemin + " tiene " + contadorvocales + " vocales y " + contadorconson + " consonantes");