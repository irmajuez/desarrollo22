let numeros=[1,2,3,4,5,-1,-2,-3,-4,-5];
let positivos=0;
let negativos=0;

for(let i=0; i<numeros.length; i++){
    if(numeros[i]<0){
        negativos+=numeros[i];
    }
    if(numeros[i]>0){
        positivos+=numeros[i];
    }
}

document.write("La suma de los números positivos es " + positivos + "<br>");
document.write("La suma de los números negativos es " + negativos);

