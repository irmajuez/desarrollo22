function suma(){
    let dato1 = document.querySelector('#inum1');
    let dato2 = document.querySelector('#inum2');

    let resultado = parseInt(dato1.value) + parseInt(dato2.value);

    document.querySelector('#resultado').value=resultado;
}

function resta(){
    let dato1 = document.querySelector('#inum3');
    let dato2 = document.querySelector('#inum4');

    let resultado = parseInt(dato1.value) - parseInt(dato2.value);

    document.querySelector('#resultado').value=resultado;
}

function negar(){
    let dato1 = document.querySelector('#inum5');

    let resultado = parseInt(dato1.value) * -1;

    document.querySelector('#resultado').value=resultado;
}

function producto(){
    let dato1 = document.querySelector('#inum6');
    let dato2 = document.querySelector('#inum7');

    let resultado = parseInt(dato1.value) * parseInt(dato2.value);

    document.querySelector('#resultado').value=resultado;
}

function cociente(){
    let dato1 = document.querySelector('#inum8');
    let dato2 = document.querySelector('#inum9');

    let resultado = parseInt(dato1.value) / parseInt(dato2.value);

    document.querySelector('#resultado').value=resultado;
}

function resto(){
    let dato1 = document.querySelector('#inum10');
    let dato2 = document.querySelector('#inum11');

    let resultado = parseInt(dato1.value) % parseInt(dato2.value);

    document.querySelector('#resultado').value=resultado;
}