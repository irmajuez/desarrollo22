/*Crear un bojeto con la clase Object*/

var persona=new Object();
persona.edad=10;
persona.nombre="Irma";
persona.decirNombre=function(){
    console.log(this.nombre);
};

/*Accedemos a los elemntos del objeto*/
console.log(persona.nombre);
persona.decirNombre();

/*Esto no se puede
hijo=new persona();
hijo.decirNombre();*/