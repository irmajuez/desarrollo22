class Animal{
    constructor(){
        this.nombrePadre="soy el padre";
    }
    
};

class Leon extends Animal{ //hereda todo de Animal
    constructor(){
        super();//ejecuta el constructor del padre y se pone siempre en la primera linea
        this.nombreHijo="soy el hijo";
    }
    
};

//una forma de añadir los miembros de animal a leon
Leon.prototype=new Animal;

var objetoPadre=new Animal();
var objetoHijo=new Leon();

console.log(objetoPadre.nombrePadre);
console.log(objetoHijo.nombrePadre);
console.log(objetoHijo.nombreHijo);